__kernel void exec(__read_only image2d_t input, __write_only image2d_t output)
{   
    const sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE;
    const int2 dim = get_image_dim(input);
    
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    uint4 color = read_imageui(input, sampler, coord).xyzw;
    
    int width = dim.x;
    int height = dim.y;

    float upperThreshold = 2.0;
    float lowerThreshold = 0.0f;
    float2 norm = (float2)(1.0f/width, 1.0f/height);
    float2 vUv = (float2)(1,-1) / norm;

    float4 currentGradientAndDirection = (float4)(color.x, color.y, color.z, color.w) / 255.f;
    float2 gradientDirection = ((currentGradientAndDirection.yz * 2.0f) - 1.0f) * norm;
    
    float firstSampledGradientMagnitude = read_imageui(input,sampler,convert_int2(gradientDirection + vUv)).x / 255.f;
    float secondSampledGradientMagnitude = read_imageui(input,sampler,convert_int2(vUv - gradientDirection)).x / 255.f;
    
    float multiplier = step(firstSampledGradientMagnitude, currentGradientAndDirection.x);
    float thresholdCompliance = smoothstep(lowerThreshold, upperThreshold, currentGradientAndDirection.x);
    
    multiplier = multiplier * step(secondSampledGradientMagnitude, currentGradientAndDirection.x);
    multiplier = multiplier * thresholdCompliance;

    write_imagef(output, coord, multiplier);
}