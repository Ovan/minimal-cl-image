__kernel void exec(__read_only image2d_t input, __write_only image2d_t output)
{
    const sampler_t sampler = CLK_FILTER_NEAREST | CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE;
    const int2 dim = get_image_dim(input);

    int2 coord = (int2)(get_global_id(0),get_global_id(1));
    
    #define I(i, j) \
        read_imageui(input, sampler, coord + (int2)(i,j)).x
    
    // threshold given by authors in their paper
    float T = 0.1 * 255;
    float x = 1.0f;
    float y = 1.0f;
        
    float a = (float)(abs(I(0, y) - I( 0, -y)) > T);        
    float b = (float)(abs(I(x, y) - I(-x, -y)) > T);
    float c = (float)(abs(I(x, 0) - I(-x,  0)) > T);
    float d = (float)(abs(I(x,-y) - I(-x, +y)) > T);
    
    float value = (a + b*2 + c*4 + d*8);
    
    write_imagef(output, convert_int2(coord), value);
}