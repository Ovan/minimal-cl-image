__kernel void exec(__read_only image2d_t input, __write_only image2d_t output)
{
    const sampler_t sampler = CLK_FILTER_NEAREST | CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE;
    const int2 dim = get_image_dim(input);

    int2 coord = (int2)(get_global_id(0),get_global_id(1));
    
    #define I(i, j) \
        read_imageui(input, sampler, coord + (int2)(i,j)).x
    
    float x = 1.0f;
    float y = 1.0f;
    float w = I(0, 0);
        
    float a = sign(I( x,  0) - w);
    float b = sign(I( x,  y) - w);
    float c = sign(I( 0,  y) - w);
    float d = sign(I(-x,  y) - w);
    float e = sign(I(-x,  0) - w);
    float f = sign(I(-x, -y) - w);
    float g = sign(I( 0, -y) - w);
    float h = sign(I( x, -y) - w);
    
    float value = (float)(a + b*2 + c*4 + d*8 + e*16 + f*32 + g*64 + h*128);
    
    write_imagef(output, convert_int2(coord), value);
}