__kernel void exec(__read_only image2d_t input, __write_only image2d_t output)
{
    const sampler_t sampler = CLK_FILTER_NEAREST | CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE;
    const int2 dim = get_image_dim(input);

    float2 coord = (float2)(get_global_id(0),get_global_id(1));
    float4 color = convert_float4(read_imageui(input,sampler,coord));

    float derivativeDifference = color.x - color.y;
    float zElement = (color.z * 2.0f) - 1.0f;

    float cornerness = color.x + color.y - sqrt(derivativeDifference * derivativeDifference + 4.0f * zElement * zElement);
    float sensitivity = -0.8f;
    float value = cornerness * sensitivity;

    write_imagef(output,convert_int2(coord), value);
}