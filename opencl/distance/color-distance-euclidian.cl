#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable

__kernel void exec(__read_only image2d_t g_frame, __global const uchar *g_ref, __write_only image2d_t g_delta)
{
    const sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE;
    const int2 dim = get_image_dim(g_frame);
    
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    uint3 color = read_imageui(g_frame, sampler, coord).xyz;
    
    float L1 = color.x;
    float a1 = color.y;
    float b1 = color.z;
    
    float L2 = g_ref[0];
    float a2 = g_ref[1];
    float b2 = g_ref[2];
    
    float delta_L = L1 - L2;
    float delta_a = a1 - a2;
    float delta_b = b1 - b2;
    
    write_imagef(g_delta,coord,
        sqrt(delta_L*delta_L + delta_a*delta_a + delta_b*delta_b)
    );
}