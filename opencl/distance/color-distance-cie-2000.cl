#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
#define pi 3.14159265359f

//! http://www2.ece.rochester.edu/~gsharma/ciede2000/dataNprograms/deltaE2000.m

__kernel void exec(__read_only image2d_t g_frame, __global const uchar *g_ref, __write_only image2d_t g_delta)
{
    const sampler_t sampler = CLK_FILTER_NEAREST | CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE;
    const int2 dim = get_image_dim(g_frame);
    
    int2 coord = (int2)(get_global_id(0), get_global_id(1));
    uint3 color = read_imageui(g_frame, sampler, coord).xyz;
    
    // The optional value of the parametric weighting factors kL, kC, and kH in paper
    
    float kl = 1.0f;
    float kc = 1.0f;
    float kh = 1.0f;
    
    float Lstd = color.x;
    float astd = color.y;
    float bstd = color.z;
    
    float Lsample = g_ref[0];
    float asample = g_ref[1];
    float bsample = g_ref[2];
    
    float Cabstd = sqrt(astd*astd + bstd*bstd);
    float Cabsample = sqrt(asample*asample + bsample*bsample);
    float Cabarithmean = (Cabstd + Cabsample) / 2.0f;
    
    float G = 0.5f - 0.5f * sqrt(
        pow(Cabarithmean, 7.0f) /
        (pow(Cabarithmean, 7.0f) + pow(25.0f, 7.0f))
    );
    
    float apstd = (1.0f + G) * astd;       // aprime in paper
    float apsample = (1.0f + G) * asample; // aprime in paper
    float Cpsample = sqrt(apsample*apsample + bsample*bsample);
    
    float Cpstd = sqrt(apstd*apstd + bstd*bstd);
    
    // Compute product of chromas and locations at which it is zero for use later
    
    float Cpprod = Cpsample * Cpstd;
    float hpstd = atan2(bstd,apstd);
    float hpsample = atan2(bsample,apsample);
    
    float dL = Lsample-Lstd;
    float dC = Cpsample-Cpstd;
    float dhp = hpsample-hpstd;
    
    // Note that the defining equations actually need
    // signed Hue and chroma differences which is different
    // from prior color difference formulae

    float dH = 2*sqrt(Cpprod)*sin(dhp/2);
    
    // weighting functions
    
    float Lp = (Lsample+Lstd)   * 0.5f;
    float Cp = (Cpstd+Cpsample) * 0.5f;
    float hp = (hpstd+hpsample) * 0.5f;
    
    float Lpm502 = pow(Lp-50.0f, 2.0f);
    float Sl = 1.0f + 0.015f*Lpm502 / sqrt(20.0f+Lpm502);  
    float Sc = 1.0f + 0.045f*Cp;
    float T = 1 - 0.17f*cos(hp - pi/6.0f) + 0.24f*cos(2.0f*hp) + 0.32f*cos(3*hp+pi/30.0f) -0.20f*cos(4*hp-63*pi/180.0f);
    float Sh = 1 + 0.015f*Cp*T;
    
    float delthetarad = (30*pi/180)*exp(-pow(180.0f / pi*hp - 275.0f/25.0f, 2.0f));
    float Rc =  2*sqrt(pow(Cp,7.0f) / (pow(Cp, 7.0f) + pow(25.0f,7.0f)));
    float RT =  - sin(2*delthetarad)*Rc;

    float klSl = kl*Sl;
    float kcSc = kc*Sc;
    float khSh = kh*Sh;

    // The CIE 2000 color difference normalized by 255/119
    // the factor is calculated to use uint8 rendering
    // the number 119 are observed in a sample scene,
    // it's the maximum of cie 2000 of the scene
    
    write_imagef(g_delta,coord,
        255.0f / 119.0f * sqrt(
        pow(dL/klSl, 2.0f) +
        pow(dC/kcSc, 2.0f) +
        pow(dH/khSh, 2.0f) +
        RT*(dC/kcSc)*(dH/khSh)
    ));
}