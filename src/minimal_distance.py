#!/usr/bni/python3
import pyopencl as cl
import numpy as np
import cv2

from glob import glob
from ColorDistance import *
from GpuTask import *
from functools import partial

def main(src, kernel):
    platforms = cl.get_platforms()
    ctx = cl.Context(properties=[(cl.context_properties.PLATFORM, platforms[0])])
    queue = cl.CommandQueue(ctx)
    
    src = cv2.cvtColor(src, cv2.COLOR_BGR2LAB)
    src = cv2.cvtColor(src, cv2.COLOR_RGB2RGBA)
    ref = np.array([[[255, 255, 255]]], dtype='uint8')
    
    task = ColorDistance(kernel, ctx, queue, src.shape)
    task.set_color_ref(cv2.cvtColor(ref, cv2.COLOR_BGR2LAB)[0,0,:])
    
    def update_render():
        task.compute(src)
        result = task.query()
        #result = result/result.max()*255
        result = result.astype('uint8')
        result = cv2.applyColorMap(result, cv2.COLORMAP_JET)
        cv2.imshow('result', result)
    pass
    
    def update_ref(chan, x):
        ref[0,0,chan] = x
        task.set_color_ref(ref)
        update_render()
    pass
    
    cv2.namedWindow('result', cv2.WINDOW_NORMAL)
    cv2.createTrackbar('L','result', ref[0,0,0], 255, partial(update_ref, 0))
    cv2.createTrackbar('A','result', ref[0,0,0], 255, partial(update_ref, 1))
    cv2.createTrackbar('B','result', ref[0,0,0], 255, partial(update_ref, 2))
    update_render()
    
    while cv2.waitKey(1) != 27 and cv2.getWindowProperty('result', cv2.WND_PROP_VISIBLE) == 1:
        pass
pass

import argparse

if __name__ == '__main__':
    kernels = glob('../opencl/distance/*.cl')

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--image", type=str, default='../data/lena.bmp',
        help="the image reference to be used"
    )
    parser.add_argument(
        "--kernel", type=str, default=kernels[0],
        help="kernel to be used",
        choices=kernels
    )
    args = parser.parse_args()
    
    image = cv2.imread(args.image)
    
    if image is None:
        print('cannot load the given image')
        exit()
    else:
        print('filename:', args.image)
        print('kernel:', args.kernel)
    pass
    
    main(image, args.kernel)
pass