#!/usr/bin/python3
import pyopencl as cl
import numpy as np
import cv2
import io

from GpuTask import *

class ColorDistance(GpuTask):

    def __init__(self, kernel, ctx, queue, shape):
        super().__init__(kernel, ctx, queue, shape)
        self.ref = np.array([255,255,255]).astype('uint8').reshape(1,1,3)
        self.g_ref = cl.Buffer(ctx, cl.mem_flags.READ_ONLY, self.ref.nbytes)
        self.set_color_ref(self.ref)
    pass
    
    #---------- functions ------------
    
    def set_color_ref(self, new_ref):
        """
        set the new color reference for computing the color distance
        it's also converted to lab and uploaded to the opencl device
        """
        self.ref = new_ref.astype('uint8').reshape(1,1,3)
        cl.enqueue_copy(self.queue, self.g_ref, self.ref).wait()
    pass
    
    def get_color_ref(self):
        """
        @return self.ref, the color used to calculate the distance
        """
        return self.ref
    pass
    
    def compute(self, frame):
        """
        upload the converted lab frame to the opencl device and
        call the micro kernel on the previously selected device
        """
        
        if self.order == 1:
            frame_lab = cv2.cvtColor(frame, cv2.COLOR_GRAY2RGBA)
        elif self.order == 2:
            frame_lab = cv2.cvtColor(frame[:,:,0], cv2.COLOR_GRAY2RGBA)
            frame_lab[:,:,3] = frame[:,:,1]
        elif self.order == 3:
            frame_lab = cv2.cvtColor(frame, cv2.COLOR_RGB2RGBA)
        else:
            frame_lab = frame
        
        #cl.enqueue_copy(self.queue, self.g_frame, frame_lab).wait()
        cl.enqueue_copy(self.queue, self.g_frame, frame_lab, origin=(0,0), region=self.shape).wait()
               
        self.prog.exec(
            self.queue,   self.shape, None,
            self.g_frame, self.g_ref, self.g_delta
        )
    pass
    
pass