#!/usr/bni/python3
import pyopencl as cl
import numpy as np
import cv2
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
from gi.repository import GdkPixbuf
from ColorDistance import *
from GpuTask import *
from glob import glob

class PyApp(Gtk.Window):
   
    def __init__(self, src):
        super(PyApp, self).__init__()
        self.set_title("PyGtk Image demo")
        self.set_size_request(300, 200)
        self.src = src
        
        self.kernels =  = glob('../opencl/estimator/*.cl')
        self.platforms = cl.get_platforms()
        self.ctx = cl.Context(properties=[(cl.context_properties.PLATFORM, self.platforms[0])])
        self.queue = cl.CommandQueue(self.ctx)
        self.task = GpuTask(self.kernels[0], self.ctx, self.queue, self.src.shape)
        
        ######################

        name_store = Gtk.ListStore(int, str)
        for i,ker in enumerate(self.kernels):
            name_store.append([i, ker.split('/')[-1]])
            
        name_combo = Gtk.ComboBox.new_with_model(name_store)
        renderer_text = Gtk.CellRendererText()
        name_combo.pack_start(renderer_text, True)
        name_combo.add_attribute(renderer_text, "text", 1)
        name_combo.set_active(0)
        name_combo.connect("changed", self.on_name_combo_changed)
        
        self.image = Gtk.Image()
        self.on_name_combo_changed(None)
        self.update_result()
        
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        vbox.pack_start(name_combo, False, False, 0)
        vbox.pack_start(self.image, True, True, 0)
        
        self.add(vbox)
        self.connect("destroy", Gtk.main_quit)
        self.show_all()
    pass
    
    def on_name_combo_changed(self, combo):
        if combo is not None:
            tree_iter = combo.get_active_iter()
            model = combo.get_model()
            row_id, name = model[tree_iter][:2]
        else:
            row_id = 0
        #self.task = GpuTask(self.kernels[row_id], self.ctx, self.queue, self.src.shape)
        self.task.set_kernel(self.kernels[row_id])
        self.update_result()
    pass
    
    def set_image(self, frame):
        pb = GdkPixbuf.Pixbuf.new_from_data(
            frame.tostring(), GdkPixbuf.Colorspace.RGB,
            False, 8, frame.shape[1], frame.shape[0],
            frame.shape[2]*frame.shape[1]
        )
        self.image.set_from_pixbuf(pb.copy())
    pass
    
    def update_result(self):
        self.task.compute(cv2.cvtColor(self.src, cv2.COLOR_BGR2XYZ))
        result = self.task.query()
        result = result/result.max()*255
        result = result.astype('uint8')
        result = cv2.applyColorMap(result, cv2.COLORMAP_JET)
        self.set_image(result)
    pass
pass

import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--image", type=str, default='../data/lena.bmp',
        help="the image reference to be used"
    )
    args = parser.parse_args()
    
    image = cv2.imread(args.image)
    
    if image is None:
        print('cannot load the given image')
        exit()
    else:
        # limit the image size for Pixbuf capacity
        while image.shape[0] > 800 or image.shape[1] > 600:
            image = cv2.resize(image, dsize=(image.shape[1]//2,image.shape[0]//2))
        print('filename:', args.image)
    pass
    
    PyApp(image)
    Gtk.main()
pass