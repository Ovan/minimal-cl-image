#!/usr/bni/python3
import pyopencl as cl
import numpy as np
import cv2

from glob import glob
from ColorDistance import *
from GpuTask import *

def main(src, kernel):
    platforms = cl.get_platforms()
    ctx = cl.Context(properties=[(cl.context_properties.PLATFORM, platforms[0])])
    queue = cl.CommandQueue(ctx)
    
    #src = cv2.cvtColor(src, cv2.COLOR_BGR2LAB)
    src = cv2.cvtColor(src, cv2.COLOR_RGB2RGBA)
    
    task = GpuTask(kernel, ctx, queue, src.shape)
    task.compute(src)
    
    result = task.query()
    result = result/result.max()*255
    result = result.astype('uint8')
    result = cv2.applyColorMap(result, cv2.COLORMAP_JET)
    
    cv2.namedWindow('result', cv2.WINDOW_NORMAL)
    cv2.imshow('result', result)
    
    while cv2.waitKey(1) != 27 and cv2.getWindowProperty('result', cv2.WND_PROP_VISIBLE) == 1:
        pass
pass

import argparse

if __name__ == '__main__':
    kernels = glob('../opencl/estimator/*.cl')

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--image", type=str, default='../data/lena.bmp',
        help="the image reference to be used"
    )
    parser.add_argument(
        "--kernel", type=str, default=kernels[0],
        help="kernel to be used",
        choices=kernels
    )
    args = parser.parse_args()
    
    image = cv2.imread(args.image)
    
    if image is None:
        print('cannot load the given image')
        exit()
    else:
        print('filename:', args.image)
        print('kernel:', args.kernel)
    pass
    
    main(image, args.kernel)
pass