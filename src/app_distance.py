#!/usr/bni/python3
import pyopencl as cl
import numpy as np
import cv2
import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from gi.repository import GObject
from ColorDistance import *
from GpuTask import *
from glob import glob

class PyApp(Gtk.Window):
   
    def __init__(self, src):
        super(PyApp, self).__init__()
        self.set_title("PyGtk Image demo")
        self.set_size_request(300, 200)
        self.src = src
        
        self.kernels = glob('../opencl/distance/*.cl')
        
        self.platforms = cl.get_platforms()
        self.ctx = cl.Context(properties=[(cl.context_properties.PLATFORM, self.platforms[0])])
        self.queue = cl.CommandQueue(self.ctx)
        self.task = None
        
        ######################

        name_store = Gtk.ListStore(int, str)
        for i,ker in enumerate(self.kernels):
            name_store.append([i, ker.split('/')[-1]])
        renderer_text = Gtk.CellRendererText()
        self.name_combo = Gtk.ComboBox.new_with_model(name_store)
        self.name_combo.set_active(0)
        self.name_combo.pack_start(renderer_text, True)
        self.name_combo.add_attribute(renderer_text, "text", 1)
        self.name_combo.connect("changed", self.on_name_combo_changed)
        
        space_store = Gtk.ListStore(int, str)
        space_store.append([cv2.COLOR_BGR2RGB, 'RGB'])
        space_store.append([cv2.COLOR_BGR2LAB, 'LAB'])
        space_store.append([cv2.COLOR_BGR2HLS, 'HLS'])
        space_store.append([cv2.COLOR_BGR2HSV, 'HSV'])
        space_store.append([cv2.COLOR_BGR2YUV, 'YUV'])
        space_store.append([cv2.COLOR_BGR2XYZ, 'XYZ'])
        renderer_text = Gtk.CellRendererText()
        self.space_combo = Gtk.ComboBox.new_with_model(space_store)
        self.space_combo.set_active(0)
        self.space_combo.pack_start(renderer_text, True)
        self.space_combo.add_attribute(renderer_text, "text", 1)
        self.space_combo.connect("changed", self.on_space_combo_changed)
        self.color_space = cv2.COLOR_BGR2RGB
        
        self.image = Gtk.Image()
        
        self.color = Gtk.ColorChooserWidget(show_editor=True, use_alpha=False)
        color = Gdk.RGBA()
        color.blue = 5/255
        color.green = 64/255
        color.red = 128/255
        color.alpha = 1.0
        self.color.set_rgba(color)
        
        self.task = ColorDistance(self.kernels[0], self.ctx, self.queue, self.src.shape)
        self.on_name_combo_changed(None)
        self.update_result()
        
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        vbox.pack_start(self.name_combo, False, False, 0)
        vbox.pack_start(self.color, True, False, 0)
        vbox.pack_start(self.space_combo, False, False, 0)
        
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        hbox.pack_start(self.image, False, False, 0)
        hbox.pack_start(vbox, False, False, 0)
        
        self.add(hbox)
        self.connect("destroy", Gtk.main_quit)
        self.show_all()
    pass
    
    def on_space_combo_changed(self, combo):
        if combo is not None:
            tree_iter = combo.get_active_iter()
            model = combo.get_model()
            self.color_space, name = model[tree_iter][:2]
        pass
    pass
    
    def on_name_combo_changed(self, combo):
        if combo is not None:
            tree_iter = combo.get_active_iter()
            model = combo.get_model()
            row_id, name = model[tree_iter][:2]
        else:
            row_id = 0
        self.task.set_kernel(self.kernels[row_id])
    pass
    
    def set_image(self, frame):
        pb = GdkPixbuf.Pixbuf.new_from_data(
            frame.tostring(), GdkPixbuf.Colorspace.RGB,
            False, 8, frame.shape[1], frame.shape[0],
            frame.shape[2]*frame.shape[1]
        )
        self.image.set_from_pixbuf(pb.copy())
    pass
    
    def update_result(self):
        color = np.array([[[
            self.color.get_rgba().blue*255,
            self.color.get_rgba().green*255,
            self.color.get_rgba().red*255,
        ]]], dtype='uint8')
        
        color = cv2.cvtColor(color, self.color_space)
        image = cv2.cvtColor(self.src, self.color_space)
        
        self.task.set_color_ref(color[0,0,:])
        self.task.compute(image)
        
        result = self.task.query()
        result = result/result.max()*255
        result = result.astype('uint8')
        result = cv2.applyColorMap(result, cv2.COLORMAP_JET)
        self.set_image(result)
        
        GObject.timeout_add(30, self.update_result)
    pass
pass

import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--image", type=str, default='../data/lena.bmp',
        help="the image reference to be used"
    )
    args = parser.parse_args()
    
    image = cv2.imread(args.image)
    
    if image is None:
        print('cannot load the given image')
        exit()
    else:
        # limit the image size for Pixbuf capacity
        while image.shape[0] > 800 or image.shape[1] > 600:
            image = cv2.resize(image, dsize=(image.shape[1]//2,image.shape[0]//2))
        print('filename:', args.image)
    pass
    
    PyApp(image)
    Gtk.main()
pass