#!/usr/bin/python3
import pyopencl as cl
import numpy as np
import cv2
import io

class GpuTask:

    def __init__(self, kernel, ctx, queue, shape):
        self.ctx = ctx
        self.queue = queue
        self.shape = np.flip(shape[0:2])
        self.order = 1 if len(shape) == 2 else shape[-1]
        
        self.set_kernel(kernel)
        self.delta = np.zeros(shape[0:2], dtype=np.float32)
        
        fmt_lab = cl.ImageFormat(cl.channel_order.RGBA, cl.channel_type.UNSIGNED_INT8)
        fmt_out = cl.ImageFormat(cl.channel_order.R, cl.channel_type.FLOAT)
          
        self.g_frame = cl.Image(self.ctx, cl.mem_flags.READ_ONLY, fmt_lab, shape=self.shape)
        self.g_delta = cl.Image(self.ctx, cl.mem_flags.WRITE_ONLY, fmt_out, shape=self.shape)
    pass
    
    def set_kernel(self, kernel):
        self.kernel = kernel
        self.prog = cl.Program(
            self.ctx, io.open(kernel).read()
        ).build()
    pass
    
    def compute(self, frame):
        """
        upload the frame to the opencl device and
        call the micro kernel on the previously selected device
        """
        
        if self.order == 1:
            frame_lab = cv2.cvtColor(frame, cv2.COLOR_GRAY2RGBA)
        elif self.order == 2:
            frame_lab = cv2.cvtColor(frame[:,:,0], cv2.COLOR_GRAY2RGBA)
            frame_lab[:,:,3] = frame[:,:,1]
        elif self.order == 3:
            frame_lab = cv2.cvtColor(frame, cv2.COLOR_RGB2RGBA)
        else:
            frame_lab = frame
        
        #cl.enqueue_copy(self.queue, self.g_frame, frame_lab).wait()
        cl.enqueue_copy(self.queue, self.g_frame, frame_lab, origin=(0,0), region=self.shape).wait()
        
        self.prog.exec(
            self.queue,   self.shape, None,
            self.g_frame, self.g_delta
        )
    pass
    
    def query(self):
        """
        query the device memorie and copie back to the cpu mem inside self.delta
        @return self.delta
        """
        cl.enqueue_copy(self.queue, self.delta, self.g_delta, origin=(0,0), region=self.shape).wait()
        return self.delta
    pass
    
pass